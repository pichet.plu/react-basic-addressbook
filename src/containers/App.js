import React, { Component } from 'react';
import { Form, Contacts } from '../components';

class App extends Component {
  state = {
    contacts: [
      { name: 'Pichet', address: '1234/567' }
    ]
  }
  createContract = (contact) => {
    this.setState({
      contacts: [...this.state.contacts, contact]
    })
  }

  render() {
    return (
      <div className="container">
        <Form onSubmit={this.createContract} />
        <hr />
        <Contacts {...this.state} />
      </div>
    );
  }
}

export default App;
